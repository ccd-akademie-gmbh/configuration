﻿using System;
using dotenv.net;

namespace configuration;

public static class Config
{
    public static void Load() {
        DotEnv.Fluent().Load();
    }

    public static void Load(string filename) {
        DotEnv.Fluent().WithEnvFiles(filename).Load();
    }

    public static class MongoDb
    {
        private static string? _host;
        private static int? _port;

        public static string Host {
            get => _host ?? GetEnvironmentVariable("MONGODB_HOST");
            set => _host = value;
        }
        
        public static int Port {
            get => _port ?? int.Parse(GetEnvironmentVariable("MONGODB_PORT"));
            set => _port = value;
        }

        public static string User => GetEnvironmentVariable("MONGODB_USER");
        
        public static string Password => GetEnvironmentVariable("MONGODB_PASSWORD");
    }
    
    private static string GetEnvironmentVariable(string variable) {
        var value = Environment.GetEnvironmentVariable(variable);
        if (value != null) {
            return value;
        }
        Load();
        value = Environment.GetEnvironmentVariable(variable);
        if (value != null) {
            return value;
        }
        return "";
    }
}