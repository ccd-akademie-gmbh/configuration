namespace configuration.tests;

public class ConfigTests
{
    [Test]
    public void Values_are_read_from_env_file() {
        Config.Load(".env");
        Assert.That(Config.MongoDb.Host, Is.EqualTo("127.0.0.1"));
        Assert.That(Config.MongoDb.Port, Is.EqualTo(27017));
        Assert.That(Config.MongoDb.User, Is.EqualTo("mongo"));
        Assert.That(Config.MongoDb.Password, Is.EqualTo("secret"));
    }
}